<?php

namespace App\Http\Resources;

use App\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class BoardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => new CategoryResource(Category::find($this->category_id)),
            'ideas' => IdeaResource::collection($this->ideas),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
