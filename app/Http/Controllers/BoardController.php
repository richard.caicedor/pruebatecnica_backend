<?php

namespace App\Http\Controllers;

use App\Board;
use App\Http\Resources\BoardResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return $this->sendResponse(BoardResource::collection(Board::all()), 'OK');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'name' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $model = Board::create($request->all());

            return $this->sendResponse(new BoardResource($model), 'OK');

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e->getMessage());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Board $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board)
    {
        return $this->sendResponse(new BoardResource($board), 'OK');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Board $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Board $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'name' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $board->update($request->all());

            return $this->sendResponse(new BoardResource($board), 'OK');

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e->getMessage());

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Board $board
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Board $board)
    {
        $board->delete();

        return $this->sendResponse([], 'OK');
    }
}
