<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ideas';

    protected $fillable = [
        "board_id",
        "description",
    ];

    /**
     * Get the boad record associated with the idea.
     */
    public function board()
    {
        return $this->hasOne('App\Board', 'id', 'board_id');
    }
}
