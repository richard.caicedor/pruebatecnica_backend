<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{  
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boards';

    protected $fillable = [
        "category_id",
        "name",
        "description"
    ];

    /**
     * Get the category record associated with the board.
     */
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    /**
     * Get the ideas for the board.
     */
    public function ideas()
    {
        return $this->hasMany('App\Idea', "board_id", 'id');
    }
}
