<?php

use Illuminate\Database\Seeder;

class IdeasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ideas')->insert([
            [
                'id' => 1,
                'board_id' => 1,
                'description' => 'Meet the new requirements'
            ],
            [
                'id' => 2,
                'board_id' => 1,
                'description' => 'Build the model'
            ],
        ]);
    }
}
