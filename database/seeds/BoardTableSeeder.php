<?php

use Illuminate\Database\Seeder;

class BoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boards')->insert([
            [
                'id' => 1,
                'name' => "Board 1",
                'description' => 'I have to add tasks of the new projects',
                'category_id' => 1
            ],
            [
                'id' => 2,
                'name' => "Board 2",
                'description' => 'I have to add tasks missing',
                'category_id' => 2
            ],
            [
                'id' => 3,
                'name' => "Board 3",
                'description' => 'I have to add tasks issues',
                'category_id' => 3
            ],
        ]);
    }
}
